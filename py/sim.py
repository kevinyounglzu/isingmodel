# -*- coding: utf8 -*-
from lib import Ising
import sys
import numpy as np


def run1(size, t):
    step = size**2 * 1000
    num_of_samples = 10000
    intervel = 1000
    #step = 10
    #times = 10
    ising = Ising(size, t)
    ising.drySimu(step * 2)
    sample = []
    for i in range(num_of_samples):
        ising.drySimu(intervel)
        sample.append((ising.averageS, ising.energy))

    np.savetxt("./%d/%f.txt" % (size, t), np.asarray(sample))


def run2(size, t):
    step = size**2 * 1000
    ising = Ising(size, t)
    ising.drySimu(step * 10)
    result = ising.simu(step * 10)
    result = np.asarray(result)
    a, b = result.mean(axis=0)
    with open("./data/%d.txt" % size, "a") as f:
        f.write("%f %f %f\n" % (t, a, b))


if __name__ == "__main__":
    _, size, t = sys.argv
    size = int(size)
    t = float(t)
    a = run2(size, t)
