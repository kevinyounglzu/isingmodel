LOGFILE=./log.log
CONFIGFILE=config
JOBS=-j1

CC=gcc
CFLAGS=-Iinclude -g -O2 -Wall -Wextra $(OPTFLAGS)
LIBS=-ldl -lm $(OPTLIBS)

TARGET_SRC=src/aver.c
TARGET=$(patsubst %.c,%,$(TARGET_SRC))
TARGET_OBJ=$(patsubst %.c,%.o,$(TARGET_SRC))

SOURCE=$(wildcard include/*.c)

$(TARGET): $(TARGET_SRC) $(SOURCE)
	$(CC) $(TARGET_SRC) $(SOURCE) $(CFLAGS) -o $(TARGET) $(LIBS)

run: $(TARGET)
	./$(TARGET)

prun: $(TARGET)
	parallel --progress --joblog $(LOGFILE) $(JOBS) --colsep ' ' ./$(TARGET) :::: $(CONFIGFILE)

prundry: $(TARGET)
	parallel --dry-run --progress --joblog $(LOGFILE) $(JOBS) --colsep ' ' ./$(TARGET) :::: $(CONFIGFILE)

genconfig:
	rm -f $(CONFIGFILE)
	python configgen.py

path:
	mkdir -p ./data/aver/20
	mkdir -p ./data/aver/40
	mkdir -p ./data/aver/80
	mkdir -p ./data/aver/160

clean:
	rm -rf $(TARGET)
	rm $(CONFIGFILE)

.PHONY: path clean
