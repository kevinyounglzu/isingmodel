# -*- coding: utf8 -*-
import numpy as np


def sweep():
    sizes = [20, 40, 80, 160]
    ts = np.linspace(1, 3.5, 50)
    with open("config", "w") as f:
        for size in sizes:
            for t in ts:
                f.write("%d %f\n" % (size, t))


if __name__ == "__main__":
    sweep()
