#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "ising.h"

int main (int argc, char *argv[])
{
    int L = atoi(argv[1]);
    double t = atof(argv[2]);
    int i = 0;
    int j = 0;
    int samples = 1000;
    int mc_step = L * L ;
    int step = mc_step * 1000;
    FILE *fp;
    char file_name[100];

    double result[L];

    srand(time(NULL));
    generateRandom();
    generateRandom();
    generateRandom();

    IsingCor * model = IsingCor_create(L, t);

    for(i=0; i<step * 20; i++)
    {
        IsingCor_one_trial(model);
    }

    for(i=0; i<samples; i++)
    {
        for(j=0; j<mc_step; j++)
        {
            IsingCor_one_trial(model);
        }
        IsingCor_set_cor(model);
    }


    IsingCor_stat(model, samples, result);

    sprintf(file_name, "./data/cor/%d/%f.txt", L, t);
    fp = fopen(file_name, "w");
    if(NULL == fp)
    {
        printf("null file\n");
    }

    for(i=0; i<L; i++)
    {
//        printf("%f\n", result[i]);
        fprintf(fp, "%f \n", result[i]);
    }

    fclose(fp);

    IsingCor_destroy(model);

    return EXIT_SUCCESS;
}
/* ----------  end of function main  ---------- */
